# Laravel (configuratie)

## Routes opvangen

1. Ga naar routes/web.php in het laravel project
2. Voeg nu dit stukje code toe om te zorgen dat alle routes linken naar index.php (routes worden gehandled in angular)
```
Route::any('{catchall}', function(){
	return View::make('index');
})->where('catchall', '.*');
```

## Linken met databank

1. Ga naar de .env file in de root van uw laravel project
2. Pas de username en wachtwoord van de databank aan naar uw eigen databank

[Terug naar README.md](../README.md)