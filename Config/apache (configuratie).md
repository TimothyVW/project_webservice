# Apache (configuratie)

1. Klik in de rij van Apache op config
2. Klik op httpd.conf
3. Verander de Directory en Document root naar uw eigen site root (ipv htdocs)
4. Voor https moet u de stappen volgen in [https (configuratie).md](https (configuratie).md)

* [Terug naar apache (installatie).md](../Doc/apache (installatie).md)
* [Terug naar README.md](../README.md)