# Titel
Kastenplanner

## Naam
Timothy Van Wichelen

### Omschrijving
Een web app die gebruikt kan worden om te visualiseren wat er nog in welke kast zit.

#### Links naar andere markdowns.

##### Doc
* [website handleiding](Doc/website handleiding.md)
* [Angular (installatie)](Doc/angular (installatie).md)
* [Apache (installatie)](Doc/apache (installatie).md)
* [angular deployment](Doc/angular deployment.md)
* [laravel (installatie)](Doc/laravel (installatie).md)

##### Config
* [apache (configuratie)](Config/apache (configuratie).md)
* [https (configuratie)](Config/https (configuratie).md)
* [laravel (configuratie)](Config/laravel (configuratie).md)