<?php

use Illuminate\Database\Seeder;

class Inhouds_seeder extends Seeder
{
    public function run()
    {
        App\Inhoud::insert([
            'kast_id' => 1,
            'aantal' => 1,
            'naam' => 'van de boom'
        ]);
        App\Inhoud::insert([
            'kast_id' => 1,
            'aantal' => 1000,
            'naam' => 'in de boom'
        ]);
        App\Inhoud::insert([
            'kast_id' => 2,
            'aantal' => 1,
            'naam' => 'sinus'
        ]);
        App\Inhoud::insert([
            'kast_id' => 2,
            'aantal' => 1,
            'naam' => 'cosinus'
        ]);
        App\Inhoud::insert([
            'kast_id' => 2,
            'aantal' => 1,
            'naam' => 'tangens'
        ]);
        for($i = 1 ; $i <= 40; $i++) {
            App\Inhoud::insert([
                'kast_id' => rand(3,10),
                'aantal' => rand(1,50),
                'naam' => 'seed item: '. $i
            ]);
        }
    }
}
