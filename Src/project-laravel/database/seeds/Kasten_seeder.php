<?php

use Illuminate\Database\Seeder;

class Kasten_seeder extends Seeder
{
    public function run()
    {
        App\Kast::insert(['title' => 'kast-anje']);
        App\Kast::insert(['title' => 'sos-kast-oa']);
        for($i = 3 ; $i <= 10; $i++) {
            App\Kast::insert([
                'title' => 'kast nr ' . $i
            ]);
        }
    }
}
 