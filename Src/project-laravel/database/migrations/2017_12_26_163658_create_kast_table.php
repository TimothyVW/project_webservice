<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //protected $table = 'kasten';
    public function up()
    {
        Schema::create('kasten', function (Blueprint $table) {  //hoe de table er moet uitzien
            $table->increments('id');
            $table->string('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kasten');
    }
}
