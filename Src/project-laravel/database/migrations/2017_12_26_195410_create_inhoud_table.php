<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInhoudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inhouds', function (Blueprint $table) { //hoe de table er moet uitzien
            $table->increments('id');
            $table->integer('kast_id');
            $table->integer('aantal');
            $table->string('naam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inhouds');
    }
}

/*
INSERT INTO `inhouds` (`id`, `kast_id`, `aantal`, `naam`) VALUES (NULL, '1', '8', 'acht');
INSERT INTO `inhouds` (`id`, `kast_id`, `aantal`, `naam`) VALUES (NULL, '2', '5', 'vijf');
INSERT INTO `inhouds` (`id`, `kast_id`, `aantal`, `naam`) VALUES (NULL, '1', '2', 'cola');
INSERT INTO `kasten` (`id`, `title`) VALUES ('1', 'ijskast');
INSERT INTO `kasten` (`id`, `title`) VALUES ('2', 'kast2');
 */

//App\kast::find(1)->inhouds