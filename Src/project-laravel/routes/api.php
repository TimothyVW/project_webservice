<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('/kast', [
    'uses' => 'KastenController@postKast'
]);

Route::get('/kasten', [
    'uses' => 'KastenController@getKasten'
]);

Route::put('/kast/{id}', [
    'uses' => 'KastenController@putKast'
]);

Route::delete('/kast/{id}', [
    'uses' => 'KastenController@deleteKast'
]);