<?php

namespace App\Http\Controllers;

use App\Inhoud;
use App\Kast;
use Illuminate\Http\Request;
class KastenController extends Controller {
    public function postKast(Request $request) {
        $kast = new Kast();
        $kast->title = $request->input('title');    //naam uit de request aan kast geven
        $kast->save();  //kast opslaan -> naar sql omzetten
        return response()->json(['kast' => $kast], 201);    //nieuwe kast returnen
    }

    public function getKasten() {
        $kasten = Kast::with('inhouds')->get(); //de hele json string inlezen, met de geneste strings included
        $response = ['kasten' => $kasten];
        return response()->json($response, 200);    //de volledige json string terugsturen (laravel wrapt deze in een object, niet vergeten unpacken in angular!)
    }

    public function putKast(Request $request, $id) {
        $kast = Kast::with('inhouds')->get()->find($id);    //heel de json string inlezen
        if(!$kast)
            return response()->json(['message' => 'Not found :('], 404);

        $deleteInhoud = Inhoud::where('kast_id', $id)->get();   //alle inhouds van deze kast inlezen
        if(!$deleteInhoud)
            return response()->json(['message' => 'gene deleteinhoud'], 404);


        foreach($deleteInhoud as $deleteThis)
        {
            $deleteThis->delete();  //eerst alle contents deleten
        }

        foreach($request->inhouds as $reqInhoud)    //de nieuwe inhouds in kast steken
        {
            $newInhoud = new Inhoud();
            $newInhoud->naam = $reqInhoud['naam'];
            $newInhoud->aantal = $reqInhoud['aantal'];
            $newInhoud->kast_id = $id;
            $kast->inhouds->push($newInhoud);
            $newInhoud->save(); //inhoud in sql table zetten
        }


        $kast->title = $request->input('title');    //titel veranderen als deze aangepast is
        $kast->save();
        return response()->json(['kast' => $kast], 200);
    }

    public function deleteKast($id) {
        $kast = Kast::find($id);
        $kast->delete();    //de kast deleten
        $deleteInhoud = Inhoud::where('kast_id', $id)->get();
        foreach($deleteInhoud as $deleteThis)   //ook de inhoud van de kast moet weg
        {
            $deleteThis->delete();
        }
        return response()->json(['message' => 'deleted'], 200);
    }
}