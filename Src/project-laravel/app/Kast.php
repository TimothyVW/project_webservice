<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kast extends Model    //(hoofdletter belangrijk)
{
    protected $table = 'kasten';    //meervoud van kast is kasten, niet kasts
    public $timestamps = false; //ik wil geen automatische timestamps

    public function inhouds()
    {
        return $this->hasMany('App\inhoud', 'kast_id'); //relatie geven: een kast heeft meerdere inhouds
    }
}
