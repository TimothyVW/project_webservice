import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowKastenComponent } from './show-kasten.component';

describe('ShowKastenComponent', () => {
  let component: ShowKastenComponent;
  let fixture: ComponentFixture<ShowKastenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowKastenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowKastenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
