import { Component, OnInit, OnChanges } from '@angular/core';
import {DataserviceService} from '../dataservice.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {Event} from '@angular/router';

@Component({
  selector: 'app-show-kasten',
  templateUrl: './show-kasten.component.html',
  styleUrls: ['./show-kasten.component.css']
})
export class ShowKastenComponent implements OnInit {
//  kasten: {id: number, title: string}[] = [];
  kasten: Kast[] = [];

  constructor(private data: DataserviceService, private router: Router) {}

  ngOnInit() {
    /***********/
    // this.data.printHello();
    this.data.getKasten().subscribe(
      (kasten) => {
      this.kasten = kasten.kasten;
      console.log(this.kasten);
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log(' get gedaan!');
      }
    );
  }
}

interface Kast {
  id: number;
  title: string;
  inhouds: Voedsel[];
}

interface Voedsel {
  naam: string;
  aantal: number;
}
/*
this.route.params
.subscribe(
(params: Params) => {
this.isbn = params['isbn'];
}
);


 */