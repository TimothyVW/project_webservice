import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ShowKastenComponent } from './show-kasten/show-kasten.component';
import { HttpClientModule } from '@angular/common/http';

import { RouterModule, Routes } from '@angular/router';

import { DataserviceService } from './dataservice.service';
import { NavbarComponent } from './navbar/navbar.component';
import { CreateKastenComponent } from './create-kasten/create-kasten.component';
import { DeleteKastenComponent } from './delete-kasten/delete-kasten.component';
import { EditKastenComponent } from './edit-kasten/edit-kasten.component';

import { FormsModule} from '@angular/forms';

const appRoutes: Routes = [
    { path: '', component: ShowKastenComponent },
    { path: 'create', component: CreateKastenComponent },
    { path: 'delete', component: DeleteKastenComponent },
    { path: 'edit', component: EditKastenComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ShowKastenComponent,
    NavbarComponent,
    CreateKastenComponent,
    DeleteKastenComponent,
    EditKastenComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, RouterModule.forRoot(appRoutes), FormsModule
  ],
  providers: [DataserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
