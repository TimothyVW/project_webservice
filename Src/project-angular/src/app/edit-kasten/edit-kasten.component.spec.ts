import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditKastenComponent } from './edit-kasten.component';

describe('EditKastenComponent', () => {
  let component: EditKastenComponent;
  let fixture: ComponentFixture<EditKastenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditKastenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditKastenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
