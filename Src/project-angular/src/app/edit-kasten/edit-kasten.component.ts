import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../dataservice.service';
import { Router } from '@angular/router';
//import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-edit-kasten',
  templateUrl: './edit-kasten.component.html',
  styleUrls: ['./edit-kasten.component.css']
})
export class EditKastenComponent implements OnInit {

    kasten: Kast[] = [];
    newInhoud: Voedsel;
    constructor(private data: DataserviceService, private router: Router) {
    }

    ngOnInit() {
        this.data.getKasten().subscribe(
            (kasten) => {
                this.kasten = kasten.kasten;
                console.log(this.kasten);
            },
            (err) => {
                console.log(err);
            },
            () => {
                console.log(' get gedaan!');
            }
        );
    }
    removeFromInhoud(kast: Kast, itemNumber: number) {
      kast.inhouds.splice(itemNumber, 1);
    }
    addToInhoud(kast: Kast, newAmount, newItemName) {
     this.newInhoud = {naam: newItemName.value, aantal: newAmount.value};
     kast.inhouds.push(this.newInhoud);
     newItemName.value = '';
     newAmount.value = '';
    }
    /*******/
    editKast(kast: Kast) {
        this.data.editKast(kast.id, kast);
    }
}

interface Kast {
    id: number;
    title: string;
    inhouds: Voedsel[];
}

interface Voedsel {
    naam: string;
    aantal: number;
}
