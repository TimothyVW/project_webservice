import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteKastenComponent } from './delete-kasten.component';

describe('DeleteKastenComponent', () => {
  let component: DeleteKastenComponent;
  let fixture: ComponentFixture<DeleteKastenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteKastenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteKastenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
