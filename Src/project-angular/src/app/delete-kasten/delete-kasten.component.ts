import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../dataservice.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-delete-kasten',
  templateUrl: './delete-kasten.component.html',
  styleUrls: ['./delete-kasten.component.css']
})
export class DeleteKastenComponent implements OnInit {
  kasten: Kast[] = [];
  constructor(private data: DataserviceService, private router: Router) { }

  ngOnInit() {
      this.data.getKasten().subscribe(
          (kasten) => {
              this.kasten = kasten.kasten;
              console.log(this.kasten);
          },
          (err) => {
              console.log(err);
          },
          () => {
              console.log(' get gedaan!');
          }
      );
  }

  deleteKast(kastID: number) {
    this.data.deleteKast(kastID);

  }

}

interface Kast {
    id: number;
    title: string;
    inhouds: Voedsel[];
}

interface Voedsel {
    naam: string;
    aantal: number;
}
