import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';

@Injectable()
export class DataserviceService {
  httpObs: Observable<any>; //wordt in get en add gebruikt, hopelijk geen error?
  nieuweKast: Kast;

  constructor(private http: HttpClient, private  router: Router) {}

  printHello() {    //testfunctie (unused)
    console.log('hello');
  }

  getKasten() {
    this.httpObs = this.http.get('https://timothyvw.ddns.net/api/kasten');
    //this.httpObs = this.http.get('http://localhost:3000/kasten');
    return this.httpObs;
  }
    /*****************************************************************************************************/
  addKast(kastNaam: string) {
    this.nieuweKast = {title: '', inhouds: []};  //anders kan title niet aangepast worden
    this.nieuweKast.title = kastNaam;
    console.log(this.nieuweKast);
    this.httpObs = this.http.post('https://timothyvw.ddns.net/api/kast',
        JSON.stringify(this.nieuweKast),
        {headers: new HttpHeaders().set('Content-Type', 'application/json')}
    );
    this.httpObs.subscribe(
        (data) => {
          console.log(data);
        },
        (err: HttpErrorResponse) => {
          console.log(err);
        },
        () => {
          console.log('post gedaan');
          this.router.navigate(['/edit']);
        }
    );
  }
  /*****************************************************************************************************/
  deleteKast(kastID: number) {
      this.httpObs = this.http.delete('https://timothyvw.ddns.net/api/kast/' + kastID);
      this.httpObs.subscribe(
          (data) => {
              console.log(data);
          },
          (err: HttpErrorResponse) => {
              console.log(err);
          },
          () => {
              console.log('delete gedaan!');
              this.router.navigate(['/']);
          }
      );
  }
  /*****************************************************************************************************/
  editKast(kastID: number, kast: Kast) {
      this.httpObs = this.http.put('https://timothyvw.ddns.net/api/kast/' + kastID,
          JSON.stringify(kast),
          {headers: new HttpHeaders().set('Content-Type', 'application/json')}
          );
      this.httpObs.subscribe(
          (data) => {
              console.log(data);
          },
          (err: HttpErrorResponse) => {
              console.log(err);
          },
          () => {
              console.log('edit gedaan!');
              this.router.navigate(['/']);
          }
      );
  }
}

interface Kast {
    title: string;
    inhouds: Voedsel[];
}

interface Voedsel {
    naam: string;
    aantal: number;
}
