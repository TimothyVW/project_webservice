import { Component, OnInit } from '@angular/core';
import {DataserviceService} from '../dataservice.service';

@Component({
  selector: 'app-create-kasten',
  templateUrl: './create-kasten.component.html',
  styleUrls: ['./create-kasten.component.css']
})
export class CreateKastenComponent implements OnInit {
  constructor(private data: DataserviceService) { }
  nieuweKast: string; // ni nodig

  ngOnInit() {
  }

  createKast(kastNaam: HTMLInputElement)
  {
    console.log(kastNaam.value);
    this.nieuweKast = kastNaam.value; //ni nodig
    this.data.addKast(kastNaam.value);
    kastNaam.value = '';
  }
}
