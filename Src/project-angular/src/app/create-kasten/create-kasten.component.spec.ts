import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateKastenComponent } from './create-kasten.component';

describe('CreateKastenComponent', () => {
  let component: CreateKastenComponent;
  let fixture: ComponentFixture<CreateKastenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateKastenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateKastenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
