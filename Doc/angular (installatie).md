# Angular (installatie)

1. Download [cmder](http://cmder.net), deze cmd is leuker om mee te werken
2. Download [nodejs](https://nodejs.org/), dit is de packet manager
3. Typ ```npm install �g @angular/cli``` in cmder om angular CLI te installeren
4. ```cd``` naar de map waar u een project in wilt
5. ```ng new [projectnaam]``` om een nieuw project aan te maken
6. ```cd``` in het gemaakte project
7. ```ng serve``` om het nieuw gemaakte project te runnen en te testen
8. Surf nu naar [http://localhost:4200](http://localhost:4200) om te zien of alles werkt

[Terug naar README.md](../README.md)