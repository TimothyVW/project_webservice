# Laravel (installatie)

1. Download en installeer [composer]( https://getcomposer.org/Composer-Setup.exe), dit is de dependency manager
2. Open cmder en ```cd``` naar uw werkmap
3. ```composer create-project --prefer-dist laravel/laravel [PROJECTNAAM]``` om een project aan te maken
4. ```cd``` naar het gemaakte project
5. ```php artisan serve``` om te testen of het project goed is aangemaakt
6. U kunt nu surfen naar [http://localhost:8000](http://localhost:8000) om te zien of alles gelukt is
7. Om te zien hoe u angular deployt voor laravel: ga naar [angular deployment.md](../Doc/angular deployment.md)
8. Voor info over de configuratie: ga naar [laravel (configuratie).md](../Config/laravel (configuratie).md)


[Terug naar README.md](../README.md)