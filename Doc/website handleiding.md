# Website Handleiding

1. Home: Hier worden alle kasten gevisualiseerd
2. Create: Hier kunt u een titel voor een nieuwe kast ingeven en op Create duwen (redirect naar Edit)
3. Delete: Hier kunt u op het kruisje duwen als u een kast wilt deleten (redirect naar Home)
4. Edit: Hier kunt u de titel van een kast aanpassen of via + en X items toevoegen of verwijderen. (redirect naar Home)

[Terug naar README.md](../README.md)