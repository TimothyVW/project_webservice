# Angular deployment

1. Ga via cmder naar uw werkmap van angular (waar u ```ng serve``` deed)
2. Maak een deployScript.sh aan

````bash
#!/bin/bash

LARAVEL_DIR="D:\documenten\semester 5\webservice\project_webservice\project-laravel"


echo 'deploying Angular project with / as base'
ng build --prod --aot

echo 'copy dist to Laravel'
cp dist/* "${LARAVEL_DIR}""\public"

echo copy "${LARAVEL_DIR}"\public\index.html to "${LARAVEL_DIR}"\resources\views\index.php
mv "${LARAVEL_DIR}""\public\index.html" "${LARAVEL_DIR}""\resources\views\index.php"

echo
echo "Done! Don't forget to add the routes in web.php located in ${LARAVEL_DIR}\routes\web.php"
echo
````
3. Verander de LARAVEL_DIR bovenaan naar uw eigen dir
4. Vanaf nu kunt u het angular project AoT compilen en overzetten naar uw angular map via het commando ```bash deployScript.sh```

[Terug naar README.md](../README.md)