# Apache (installatie)

## xampp

1. Download xampp op [https://www.apachefriends.org/index.html](https://www.apachefriends.org/index.html)
2. Open xampp
3. Voor de configuratie kunt u gaan naar [apache (configuratie)](../Config/apache (configuratie).md)
4. Klik naast Apache op start om de site te starten
5. Klik op start naast MySQL om de databank te starten

[Terug naar README.md](../README.md)